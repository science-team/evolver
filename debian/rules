#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

DEB_ARCH_REVLISTOF_LIBQUAMATH ?= amd64 i386 kfreebsd-amd64 kfreebsd-i386 hurd-i386 x32

export DEB_PKG_VERSION = $(DEB_VERSION)
export UPS_PKG_VERSION = $(firstword $(subst +ds, ,$(DEB_PKG_VERSION)))
export UPS_PKG_VERSION_INT = $(subst .,,$(UPS_PKG_VERSION))

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

DEB_VIRT_EVOLVER_FLAVOUR_VARIANT_LISTOF_DHFILE = .install .manpages
DEB_VIRT_EVOLVER_FLAVOUR_FLAVOUR_LISTOF_DHFILE = .lintian-overrides .postinst .prerm .README.Debian
DEB_VIRT_EVOLVER_LISTOF_FLAVOUR = nox ogl
DEB_VIRT_EVOLVER_LISTOF_FLOATINGPOINTFORMAT = d
ifneq ($(DEB_HOST_ARCH), ppc64el)
DEB_VIRT_EVOLVER_LISTOF_FLOATINGPOINTFORMAT += ld
endif
ifneq (,$(filter $(DEB_HOST_ARCH),$(DEB_ARCH_REVLISTOF_LIBQUAMATH)))
DEB_VIRT_EVOLVER_LISTOF_FLOATINGPOINTFORMAT += q
endif

# deduced variables
DEB_VIRT_EVOLVER_FLAVOUR_LISTOF_DHFILE = \
	$(DEB_VIRT_EVOLVER_FLAVOUR_VARIANT_LISTOF_DHFILE) \
	$(DEB_VIRT_EVOLVER_FLAVOUR_FLAVOUR_LISTOF_DHFILE)
DEB_VIRT_EVOLVER_LISTOF_VARIANT = \
	$(foreach ef, $(DEB_VIRT_EVOLVER_LISTOF_FLAVOUR), \
		$(foreach fpf, $(DEB_VIRT_EVOLVER_LISTOF_FLOATINGPOINTFORMAT), \
			$(ef)-$(fpf) \
			) \
		)
#

CFLAGS := $(subst -O2,-O3,$(CFLAGS))

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	MAKEFLAGS += -j$(NUMJOBS)
endif


SED_KEY_FLAVOUR_nox = NOX
SED_KEY_FLAVOUR_ogl = GLUT
SED_KEY_FPF_d = UNDEFINED
SED_KEY_FPF_ld = LONGDOUBLE
SED_KEY_FPF_q = FLOAT128

default:
	@uscan --no-conf --dehs --report || true

%:
	dh $@ --builddirectory=_build

override_dh_auto_configure-arch:
	mkdir -p $(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT), _build/$(ev)/src )
	$(foreach ef, $(DEB_VIRT_EVOLVER_LISTOF_FLAVOUR), \
		$(foreach fpf, $(DEB_VIRT_EVOLVER_LISTOF_FLOATINGPOINTFORMAT), \
			sed \
					-e "s|#>$(SED_KEY_FLAVOUR_$(ef))<#||g" \
					-e "s|#>>$(SED_KEY_FPF_$(fpf))<<#||g" \
				src/Makefile > _build/$(ef)-$(fpf)/src/Makefile \
			$(NEWLINE) ) \
		)

override_dh_prep-arch:
	mkdir -p _build/DEBIAN
	$(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT), \
		$(eval _ef:=$(firstword $(subst -, $(EMPTY), $(ev)))) \
		$(foreach dhf, $(DEB_VIRT_EVOLVER_FLAVOUR_VARIANT_LISTOF_DHFILE), \
			sed \
					-e "s|@VARIANT@|$(ev)|g" \
				debian/templates/evolver-FLAVOUR$(dhf).in >> debian/evolver-$(_ef)$(dhf) \
			$(NEWLINE) ) \
		)
	$(foreach ef, $(DEB_VIRT_EVOLVER_LISTOF_FLAVOUR), \
		$(foreach dhf, $(DEB_VIRT_EVOLVER_FLAVOUR_FLAVOUR_LISTOF_DHFILE), \
			sed \
					-e "s|@FLAVOUR@|$(ef)|g" \
					-e "s|@LISTOF_VARIANT@|$(filter $(ef)%, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT))|g" \
				debian/templates/evolver-FLAVOUR$(dhf).in > debian/evolver-$(ef)$(dhf) \
			$(NEWLINE) ) \
		)
	$(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT), \
		$(MAKE) -C _build/DEBIAN -f ../../debian/adhoc/Makefile manpages VARIANT=$(ev) TOP_SRCDIR=../.. VPATH=../$(ev)/src $(NEWLINE) \
		)
	dh_prep -a

override_dh_auto_build-arch:
	$(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT), $(MAKE) -C _build/$(ev)/src EXEC_SUFFIX=-$(ev) VPATH=../../../src $(NEWLINE) )

override_dh_auto_build-indep:

override_dh_auto_install-arch:
	$(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_VARIANT), \
		$(MAKE) -f debian/adhoc/Makefile install-exec VARIANT=$(ev) TOP_BUILDDIR=_build/$(ev) DESTDIR=debian/tmp $(NEWLINE) \
		)

override_dh_auto_install-indep:
	$(MAKE) -f debian/adhoc/Makefile install-doc DESTDIR=debian/tmp

override_dh_link-indep:
	dh_link -p evolver-doc \
		/usr/share/doc/evolver/manual$(UPS_PKG_VERSION_INT).pdf /usr/share/doc/evolver/manual.pdf \
		/usr/share/doc/evolver/examples /usr/share/doc/evolver/fe

override_dh_compress-indep:
	dh_compress -Xevhelp.txt -X.pdf -Xexamples

override_dh_auto_clean-arch:
	dh_auto_clean -a
	rm -rf _build
	rm -f $(foreach ev, $(DEB_VIRT_EVOLVER_LISTOF_FLAVOUR), $(foreach dhf, $(DEB_VIRT_EVOLVER_FLAVOUR_LISTOF_DHFILE), debian/evolver-$(ev)$(dhf) ))

update-debian-control: debian/templates/control.in debian/rules
	sed \
			-e "s|@braCSket_DEB_ARCH_REVLISTOF_LIBQUAMATH@|[$(DEB_ARCH_REVLISTOF_LIBQUAMATH)]|g" \
		$< > debian/control


define NEWLINE


endef

EMPTY :=
EMPTY +=
