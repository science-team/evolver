Description: source: contrib: readline(3) support
 Bring to the evolver command-line interface readline(3). The original
 code written by Laszlo Csirmaz <csirmaz@renyi.hu> was imported by hand
 from GitHub (see Origin field), and then slightly modified, by Jerome
 Benoit <calculus@rezozer.net> on behalf of the Debian Science Team.
Origin: github, https://github.com/kashif/evolver
Forwarded: by email
Author: Laszlo Csirmaz <csirmaz@renyi.hu>
Last-Update: 2016-07-16

--- /dev/null
+++ b/src/readline.c
@@ -0,0 +1,198 @@
+/*************************************************************
+ *  This file is an extension for the Surface Evolver        *
+ *  Programmer: Laszlo Csirmaz, csirmaz@renyi.hu             *
+ ************************************************************/
+
+
+/************************************************************************
+*
+*   File: readline.c
+*
+*  Purpose: automatic extension by using the "readline" library
+*
+*
+*/
+
+#ifdef USE_READLINE
+
+#include <readline/readline.h>
+#include <readline/history.h>
+
+#ifndef EVOLVERHISTDOTFILENAME
+#define EVOLVERHISTDOTFILENAME ".evolver_history"
+#endif
+
+static int whspace(char c)
+{  switch(c){
+      case ' ': case '\t': case '\r': case '\n': return 1;
+      default: break;
+   }
+   return 0;
+}
+
+static char *stripwhite(char *str)
+{char *s,*t;
+    for(s=str;whspace(*s);s++);
+    if(*s==0) return s;
+    t=s+strlen(s)-1;
+    while(t>s && whspace(*t))t--;
+    *++t='\0';
+    return s;
+}
+
+/* list of extended words in alphabetical order; no single letter
+   word is extended */
+static char *head[]={
+#include "rl_head.h"
+NULL
+};
+static char *middle[]={
+#include "rl_mid.h"
+NULL
+};
+
+static char **expand_from;
+static int do_file_expand=0;
+static int save_history=1;
+static char *lastline = NULL;
+static int lastlinepos = 0;
+
+static char * match_generator(const char *txt, int state)
+{static int list_index,len; char *name;
+    if(state==0){ /* first call */
+        list_index=0;
+        len=strlen(txt);
+    }
+    while( (name=expand_from[list_index])!=NULL ){
+       list_index++;
+       if(strncmp(name,txt,len)==0)
+           return strdup(name);
+    }
+    return (char*)NULL;
+}
+
+static char **my_comp( const char *text, int start, int end)
+{
+    // the string to be completed starts at "start"
+    if(do_file_expand == 0){ /* no filename extension */
+       expand_from = start ? middle : head;
+       return rl_completion_matches(text,match_generator);
+    }
+    rl_attempted_completion_over = 0;
+    return (char **)NULL; /* filename extension */
+}
+
+static void initialize_readline(int flag)
+{	static char hist[PATHSIZE];
+	static int initialized=0;
+	if ((!(flag)) && (initialized==0))
+	{	memset(hist,'\0',PATHSIZE);
+		char *env_HOME=getenv("HOME");
+		rl_readline_name="evolver";
+		rl_attempted_completion_function=my_comp;
+		using_history(); /* initialize */
+		if (env_HOME)
+		{	strncpy(hist,env_HOME,PATHSIZE-1);
+			strncat(hist, "/" EVOLVERHISTDOTFILENAME ,PATHSIZE-1-strlen(env_HOME));
+			read_history_range(hist,0,100);
+			initialized=+1;
+		}
+		else
+		{	initialized=-1; }
+	}
+	else if ((flag) && (0<initialized))
+	{	write_history(hist);
+		*hist='\0';
+		initialized=-1;
+	}
+}
+
+void save_readline_history(void)
+{	initialize_readline(1); }
+
+static int use_readline(char *prompt, char *inmsg, int max )
+{	static int readline_uninitialized=1;
+	int len; char *s;
+    if(readline_uninitialized)
+		{	initialize_readline(0);
+			readline_uninitialized=0;
+		}
+    if(prompt == CONTPROMPT ){ /* next part of lastline */
+        s=lastline+lastlinepos;
+        len=strlen(s); strncpy(inmsg,s,max);
+        if(len >= max-1) {
+           inmsg[max-1]=MOREIN; inmsg[max]=0; len=max-1;
+        }
+        lastlinepos += len;
+        return 1;
+    }
+    if(prompt == MOREPROMPT ){ /* continuation line */
+        /* do not add to the history yet */
+        char *contline,*new;
+        contline = readline( topflag ? "" : "more> " );
+        if(!contline){ contline = strdup(""); }
+        s=stripwhite(contline);
+        new=malloc(lastlinepos+strlen(s)+2);
+        if(!new){ /* pretend got nothing */
+            free(contline);
+            s=lastline+lastlinepos;
+        } else {
+            strncpy(new,lastline,lastlinepos);
+            strcpy(new+lastlinepos," ");
+            lastlinepos++;
+            strcpy(new+lastlinepos,s);
+            free(lastline); free(contline); lastline=new;
+            s=lastline+lastlinepos;
+        }
+    } else { /* read next line */
+        if(lastline){
+           s=stripwhite(lastline);
+					 if (save_history
+					 		&& s[0] && s[1] && s[2]
+					 		&& strncmp(s,"help",4)
+					 		&& strncmp(s,"bye",3)
+					 		&& strncmp(s,"exit",4)
+					 		&& strncmp(s,"quit",4)
+					 		)
+					 {
+               HIST_ENTRY **hl; int n,i,found;
+	       /* if it is not on the history list, put it on;
+	          otherwise move the line to the last one */
+               hl=history_list(); /* get the history list */
+               found=0;
+               if(hl){ /* find the current line */
+                   i=0;
+                   for(n=0; *hl && (*hl)->line ; hl++,n++);
+                   while(found==0 && n>0 && i<10){
+                       i++; n--; hl--;
+                       if(strcmp((*hl)->line,s)==0){
+                           HIST_ENTRY *old;
+                           found=1;
+                           old=remove_history(n);
+                           if(old && old->line) free(old->line);
+                       }
+                   }
+               }
+               add_history(s);
+           }
+           free(lastline);
+        }
+        do_file_expand=0; save_history = 1;
+        if( strstr(prompt,"file") ) do_file_expand=1;
+        if( strcmp(prompt,"Graphics command: ")==0) save_history = 0;
+        lastline = readline(prompt);
+        if(!lastline) { /* EOF */
+            return EOF;
+        }
+        s=stripwhite(lastline); lastlinepos=s-lastline;
+    }
+    len=strlen(s);  strncpy(inmsg,s,max);
+    if(len >= max-1) {
+       inmsg[max-1]=MOREIN; inmsg[max]=0; len=max-1;
+    }
+    lastlinepos += len;
+    return 1;
+}
+
+
+#endif
--- /dev/null
+++ b/src/rl_head.h
@@ -0,0 +1,326 @@
+"actual_volume",
+"alice",
+"approx_curvature",
+"approximate_curvature",
+"area",
+"area_fixed",
+"area_method_name",
+"area_normalization",
+"areaweed",
+"attribute",
+//"attributes",
+"autochop",
+"autodisplay",
+"autopop",
+"autorecalc",
+"axial_point",
+"backbody",
+"backcolor",
+"bare",
+"bezier_basis",
+"bodies",
+"body",
+"bottominfo",
+//"boundaries",
+"boundary",
+"boundary_curvature",
+"break",
+"burchard",
+"bye",
+"chdir",
+"check",
+"clipped_cells",
+"close_show",
+"color",
+"colorfile",
+"conducting_knot_energy",
+"conformal_metric",
+"connected_cells",
+"conserved",
+"constraint",
+"constraint_tolerance",
+//"constraints",
+"content",
+"continue",
+"convert_to_quantities",
+"convex",
+"count",
+"counts",
+"datafilename",
+"date_and_time",
+"debug",
+"define",
+"delete",
+"density",
+"diffusion",
+"dihedral",
+"dirichlet",
+"dirichlet_mode",
+"dissolve",
+"do",
+"dump",
+//"e",
+"edge",
+"edge_divide",
+//"edges",
+"edgeswap",
+"edgeweed",
+"effective_area",
+"efixed",
+"eigenprobe",
+"element_modulus",
+"else",
+"energy",
+"eprint",
+"equiangulate",
+"errprintf",
+"estimate",
+"everything_quantities",
+"evolver_version",
+"exec",
+"exit",
+"exprint",
+"extrapolate",
+"face",
+//"faces",
+//"facet",
+"facet_edge",
+//"facet_edges",
+//"facetedge",
+"facetedges",
+//"facets",
+"fix",
+"fixed",
+"fixed_area",
+"for",
+"foreach",
+"form_integrand",
+"formula",
+"frontbody",
+"frontcolor",
+"function",
+//"g",
+"gap_constant",
+"gauss_curvature",
+"geompipe",
+"geomview",
+"global",
+"global_method",
+"go",
+"gravity",
+"gravity_constant",
+"gridflag",
+"help",
+"hessian",
+"hessian_menu",
+"hessian_seek",
+"hessian_special_normal_vector",
+"histogram",
+"history",
+"hit_constraint",
+"hit_partner",
+"homothety",
+"id",
+"if",
+"ignore_constraints",
+"ignore_fixed",
+//"imod",
+"info_only",
+"insulating_knot_energy",
+"integer",
+"integral_order",
+"integral_order_1d",
+"integral_order_2d",
+//"integration_order",
+//"integration_order_1d",
+//"integration_order_2d",
+"interp_bdry_param",
+"is_defined",
+"jiggle",
+"k_vector_order",
+"keep_macros",
+"keep_originals",
+"keylogfile",
+"klein_metric",
+"kmetis",
+"labelflag",
+"lagrange",
+"lagrange_multiplier",
+"lagrange_order",
+"lanczos",
+"length",
+"length_method_name",
+"linear",
+"list",
+"load",
+"load_library",
+"local",
+"logfile",
+"loghistogram",
+"longj",
+"mean_curvature",
+"mean_curvature_integral",
+"merit_factor",
+"method",
+"method_instance",
+"metis",
+"metis_factor",
+"metric",
+"midv",
+"mobility",
+"mobility_tensor",
+"move",
+"new_body",
+"new_edge",
+"new_facet",
+"new_vertex",
+"no_display",
+"no_refine",
+"nodisplay",
+"noncontent",
+"nonnegative",
+"nonpositive",
+"nonwall",
+"normal_curvature",
+"notch",
+"oid",
+"ometis",
+"on_boundary",
+"on_constraint",
+"ooglfile",
+"opacity",
+//"optimise",
+//"optimising_parameter",
+"optimize",
+"optimizing_parameter",
+"orientation",
+"original",
+"parameter",
+"parameter_1",
+"parameter_file",
+"parameters",
+"partner_hitting",
+"pause",
+"pdelta",
+"periods",
+"permload",
+"phase",
+"phasefile",
+//"pi",
+"pop",
+"pop_edge_to_tri",
+"pop_quad_to_quad",
+"pop_tri_to_edge",
+"postscript",
+"pressure",
+"print",
+"printf",
+"procedure",
+"procedures",
+"pscale",
+"pscolorflag",
+"quadratic",
+"quantity",
+"quiet",
+"quietgo",
+"quietload",
+"quit",
+"raw_vertex_average",
+"rawest_vertex_average",
+"rawestv",
+"rawv",
+"read",
+"real",
+"rebody",
+"recalc",
+"refine",
+"renumber_all",
+"reorder_storage",
+"return",
+"ritz",
+"runge_kutta",
+"saddle",
+"scalar_integrand",
+"scale",
+"scale_limit",
+"self",
+"set",
+"shading",
+"shell",
+"show",
+"show_all_quantities",
+"show_expr",
+"show_inner",
+"show_off",
+"show_outer",
+"show_trans",
+"show_vol",
+"showq",
+"simplex_representation",
+"sizeof",
+"soapfilm",
+"sobolev_mode",
+"sobolev_seek",
+"space_dimension",
+"spring_constant",
+"sprintf",
+"sqcurve",
+"square_curvature",
+"square_gaussian_curvature",
+"squared_curvature",
+"squared_gaussian_curvature",
+"stability_test",
+"string",
+//"sum",
+"surface_dimension",
+"surface_energy",
+"swap_colors",
+"symmetric_content",
+"symmetry_group",
+"system",
+"tag",
+"target",
+"temperature",
+"tension",
+"tetra_point",
+"then",
+"thicken",
+"tolerance",
+"topinfo",
+"torus",
+"torus_filled",
+"torus_periods",
+"total",
+"total_time",
+"transform_depth",
+"transform_expr",
+"triple_point",
+"ulong",
+"unfix",
+"unset",
+"utest",
+"valence",
+"value",
+"vector_integrand",
+"verbose",
+"vertex",
+"vertex_average",
+"vertexnormal",
+"vertices",
+"view_matrix",
+"view_transform_generators",
+"view_transforms",
+"visibility_test",
+"volconst",
+"volfixed",
+"volume",
+"volume_method_name",
+"warning_messages",
+"where",
+"while",
+"wrap",
+"wrap_vertex",
+"wulff",
+"zoom",
+"zoom_radius",
+"zoom_vertex",
--- /dev/null
+++ b/src/rl_mid.h
@@ -0,0 +1,436 @@
+"abs",
+"acos",
+"acosh",
+"actual_volume",
+"alice",
+"ambient_pressure",
+"and",
+"approx_curv",
+"approx_curvature",
+"approximate_curvature",
+"area",
+"area_fixed",
+"area_method_name",
+"area_normalization",
+"areaweed",
+"asin",
+"asinh",
+"assume_oriented",
+"atan",
+"atan2",
+"atanh",
+"attribute",
+//"attributes",
+"augmented_hessian",
+"autochop",
+"autodisplay",
+"autopop",
+"autorecalc",
+"avg",
+"axial_point",
+"backbody",
+"backcolor",
+"backcull",
+"bare",
+"bezier_basis",
+"blas_flag",
+"blue",
+"bodies",
+"body",
+"bottominfo",
+//"boundaries",
+"boundary",
+"boundary_curvature",
+"break",
+"break_after_warning",
+"bunch_kauffman",
+"bunch_kaufman",
+"burchard",
+"bye",
+"ceil",
+"chdir",
+"check",
+"check_increase",
+"circular_arc_draw",
+"clear",
+"clipped",
+"clipped_cells",
+"close_show",
+"color",
+"colorfile",
+"colormap",
+"conducting_knot_energy",
+"conf_edge",
+"conformal_metric",
+"conj_grad",
+"connected",
+"connected_cells",
+"conserved",
+"constraint",
+"constraint_tolerance",
+//"constraints",
+"content",
+"continue",
+"convert_to_quantities",
+"convex",
+"cos",
+"cosh",
+"count",
+"counts",
+"crossingflag",
+"cyan",
+"datafilename",
+"date_and_time",
+"debug",
+"define",
+"delete",
+"density",
+"deturck",
+"diffusion",
+"dihedral",
+"dirichlet",
+"dirichlet_mode",
+"dirichlet_seek",
+"dissolve",
+"div_normal_curvature",
+"do",
+"dump",
+//"e",
+"edge",
+"edge_divide",
+"edges",
+"edgeswap",
+"edgeweed",
+"effective_area",
+"efixed",
+"eigenprobe",
+"element_modulus",
+"ellipticE",
+"ellipticK",
+"else",
+"energy",
+"eprint",
+"equiangulate",
+"errprintf",
+"estimate",
+"everything_quantities",
+"evolver_version",
+"exec",
+"exit",
+"exp",
+"exprint",
+"extrapolate",
+"face",
+//"faces",
+//"facet",
+"facet_colors",
+"facet_edge",
+//"facet_edges",
+"facetedge",
+//"facetedges",
+//"facets",
+"fix",
+"fixed",
+"fixed_area",
+"floor",
+"for",
+"force_pos_def",
+"foreach",
+"form_integrand",
+"formula",
+"frontbody",
+"frontcolor",
+"function",
+//"g",
+"gap_constant",
+"gauss_curvature",
+"geompipe",
+"geomview",
+"global",
+"global_method",
+"go",
+"green",
+"gravity",
+"gravity_constant",
+"gridflag",
+"gv_binary",
+"h_inverse_metric",
+"help",
+"hessian",
+"hessian_diff",
+"hessian_double_normal",
+"hessian_menu",
+"hessian_normal",
+"hessian_normal_one",
+"hessian_normal_perp",
+"hessian_quiet",
+"hessian_seek",
+"hessian_special_normal",
+"hessian_special_normal_vector",
+"histogram",
+"history",
+"hit_constraint",
+"hit_partner",
+"homothety",
+"id",
+"idiv",
+"if",
+"ignore_constraints",
+"ignore_fixed",
+"imod",
+"incompleteEllipticE",
+"incompleteEllipticF",
+"info_only",
+"insulating_knot_energy",
+"integer",
+"integral_order",
+"integral_order_1d",
+"integral_order_2d",
+//"integration_order",
+//"integration_order_1d",
+//"integration_order_2d",
+"interp_bdry_param",
+"interp_normals",
+"is_defined",
+"itdebug",
+"jiggle",
+"k_vector_order",
+"keep_macros",
+"keep_originals",
+"keylogfile",
+"klein_metric",
+"kmetis",
+"kraynikpopedge",
+"kraynikpopvertex",
+"kusner",
+"labelflag",
+"lagrange",
+"lagrange_multiplier",
+"lagrange_order",
+"lanczos",
+"length",
+"length_method_name",
+"lightblue",
+"lightcyan",
+"lightgray",
+"lightgreen",
+"lightmagenta",
+"lightred",
+"linear",
+"linear_metric",
+"list",
+"load",
+"load_library",
+"local",
+"log",
+"logfile",
+"loghistogram",
+"longj",
+"magenta",
+"max",
+"maximum",
+"mean_curvature",
+"mean_curvature_integral",
+"memdebug",
+"merit_factor",
+"method",
+"method_instance",
+"metis",
+"metis_factor",
+"metric",
+"metric_conversion",
+"metric_convert",
+"midv",
+"min",
+"minimum",
+"mobility",
+"mobility_tensor",
+"mod",
+"modulus",
+"move",
+"new_body",
+"new_edge",
+"new_facet",
+"new_vertex",
+"no_display",
+"no_refine",
+"nodisplay",
+"noncontent",
+"nonnegative",
+"nonpositive",
+"nonwall",
+"normal_curvature",
+"normal_motion",
+"not",
+"notch",
+"off",
+"oid",
+"old_area",
+"ometis",
+"on",
+"on_boundary",
+"on_constraint",
+"ooglfile",
+"opacity",
+//"optimise",
+//"optimising_parameter",
+"optimize",
+"optimizing_parameter",
+"or",
+"orientation",
+"original",
+"parameter",
+"parameter_1",
+"parameter_file",
+"parameters",
+"partner_hitting",
+"pause",
+"pdelta",
+"periods",
+"permload",
+"phase",
+"phasefile",
+//"pi",
+"pinning",
+"pop",
+"pop_edge_to_tri",
+"pop_quad_to_quad",
+"pop_tri_to_edge",
+"post_project",
+"postscript",
+"pow",
+"pressure",
+"print",
+"printf",
+"procedure",
+"procedures",
+"pscale",
+"pscolorflag",
+"quadratic",
+"quantities_only",
+"quantity",
+//"quiet",
+//"quietgo",
+//"quietload",
+"quit",
+"raw_cells",
+"raw_vertex_average",
+"rawest_vertex_average",
+"rawestv",
+"rawv",
+"read",
+"real",
+"rebody",
+"recalc",
+"refine",
+"renumber_all",
+"reorder_storage",
+"return",
+"rgb_colors",
+"ribiere",
+"ritz",
+"runge_kutta",
+"saddle",
+"scalar_integrand",
+"scale",
+"scale_limit",
+"self",
+"self_similar",
+"set",
+"shading",
+"shell",
+"show",
+"show_all_quantities",
+"show_expr",
+"show_inner",
+"show_off",
+"show_outer",
+"show_trans",
+"show_vol",
+"showq",
+"simplex_representation",
+"sin",
+"sinh",
+"sizeof",
+"soapfilm",
+"sobolev",
+"sobolev_mode",
+"sobolev_seek",
+"space_dimension",
+"sparse_constraints",
+"spring_constant",
+"sprintf",
+"sqcurve",
+"sqgauss",
+//"sqr",
+"sqrt",
+"square_curvature",
+"square_gaussian_curvature",
+"squared_curvature",
+"squared_gaussian_curvature",
+"squared_gradient",
+"stability_test",
+"string",
+"sum",
+"surface_dimension",
+"surface_energy",
+"swap_colors",
+"symmetric_content",
+"symmetry_group",
+"system",
+"tag",
+"tan",
+"tanh",
+"target",
+"temperature",
+"tension",
+"tetra_point",
+"then",
+"thicken",
+"tolerance",
+"topinfo",
+"torus",
+"torus_filled",
+"torus_periods",
+"total",
+"total_time",
+"transform_depth",
+"transform_expr",
+"transforms",
+"triple_point",
+"ulong",
+"unfix",
+"unset",
+"utest",
+"valence",
+"value",
+"vector_integrand",
+"verbose",
+"vertex",
+"vertex_average",
+"vertexnormal",
+"vertices",
+"view_4d",
+"view_matrix",
+"view_transform_generators",
+"view_transforms",
+"visibility_test",
+"volconst",
+"volfixed",
+"volgrads_every",
+"volume",
+"volume_method_name",
+"warning_messages",
+"where",
+"while",
+"wrap",
+"wrap_compose",
+"wrap_inverse",
+"wrap_vertex",
+"wulff",
+"yellow",
+"ysmp",
+"zener_drag",
+"zoom",
+"zoom_radius",
+"zoom_vertex",
--- a/src/tmain.c
+++ b/src/tmain.c
@@ -239,11 +239,6 @@
 	if ( sizeof(element_id) > sizeof(REAL) )
     kb_error(3102,"Bad datatype sizes: element_id is %d bytes, but REAL is %d.\n",
       UNRECOVERABLE);
-      
-#ifdef _HISTORY_H_
- /* readline history initialization */
- using_history(); 
-#endif
 
 #ifdef SGI_MULTI
   procs_requested = 1;  /* default at John's request.  m_get_numprocs(); */
--- a/src/grapher.c
+++ b/src/grapher.c
@@ -212,14 +212,19 @@
 *
 */
 
+#ifdef USE_READLINE
+#define EVOLVER_GCI_SKIPSET "0123456789.+-udrlcCzsRmxqtBvwbeET?h^'_,><\n\r"
+#else
+#define EVOLVER_GCI_SKIPSET "0123456789.+-udrlcCRmzsABDxqtvwbeETH?h()onf\034\035\036\037\033\133\n\r"
+#endif
+
 int view_transform(char *string)
 {
   char *c;
   size_t legal; /* number of legal characters at start of string */
 
   /* test for illegal characters */
-  legal =  strspn(string,
-          "0123456789.+-udrlcCRmzsABDxqtvwbeETH?h()onf\034\035\036\037\033\133\n\r");
+  legal =  strspn(string, EVOLVER_GCI_SKIPSET );
   if ( legal != strlen(string) )
   { sprintf(msg,"Illegal character in graphics command: %c",string[legal]);
     kb_error(1044,msg,WARNING);
@@ -296,26 +301,43 @@
          mat_mult(zoom,view,view,HOMDIM,HOMDIM,HOMDIM); break;  
 
         /* MS-DOS arrow keys for translation */
-        case 30: 
+#ifdef USE_READLINE
+				case '^' : case '\'' :
+#else
+        case 30:
+#endif
          transup[SDIM>2?2:1][HOMDIM-1] = decflag ? val : 0.25;
          mat_mult(transup,  view,view,HOMDIM,HOMDIM,HOMDIM);
          break;
 
-        case 31: 
+#ifdef USE_READLINE
+				case '_' : case ',' :
+#else
+        case 31:
+#endif
          transdown[SDIM>2?2:1][HOMDIM-1] = decflag ? -val : -0.25;
          mat_mult(transdown,view,view,HOMDIM,HOMDIM,HOMDIM);
          break;
 
-        case 28: 
+#ifdef USE_READLINE
+				case '>' :
+#else
+        case 28:
+#endif
          transright[SDIM>2?1:0][HOMDIM-1] = decflag ? val : 0.25;
          mat_mult(transright,view,view,HOMDIM,HOMDIM,HOMDIM);
          break;
 
-        case 29: 
+#ifdef USE_READLINE
+				case '<' :
+#else
+        case 29:
+#endif
          transleft[SDIM>2?1:0][HOMDIM-1] = decflag ? -val : -0.25;
          mat_mult(transleft,view,view,HOMDIM,HOMDIM,HOMDIM);
          break;
 
+#if !defined(USE_READLINE)
         case 0x1b : /* ANSI arrow keys for translation */
           if ( *(++c) != 0x5B )
              { if ( isprint(*c) )
@@ -354,6 +376,7 @@
                      break; 
              }
           break;
+#endif /* !defined(USE_READLINE) */
 
         case 'R':
           if ( decflag ) /* particular scaling */
@@ -380,7 +403,7 @@
           }
 
         case 'x':
-        case 'q': return 0;      
+        case 'q': return 0;
 
         case 't': 
               if ( !web.symmetry_flag ) break;
@@ -464,7 +487,7 @@
                   graph_timestamp = ++global_timestamp;
                   reps = 0; break;
 
-        case '?': 
+        case '?':
         case 'h':
                   graph_help();
                   showflag = 0;
@@ -485,6 +508,8 @@
   return 1;
 } /* end view_transform() */
 
+#undef EVOLVER_GCI_SKIPSET
+
 /********************************************************************
 *
 * function: init_view()
--- a/src/help.c
+++ b/src/help.c
@@ -122,20 +122,32 @@
 " C    Rotate counterclockwise 6 degrees.",
 " z    Zoom by factor of 1.2.",
 " s    Shrink by factor of 1.2.",
+#ifdef USE_READLINE
+" >    Shift image right.",
+" ^,'  Shift image up.",
+" <    Shift image left.",
+" _,,  Shift image down.",
+#else
 " arrow keys - Translate image.",
+#endif
 " m    Center image.",
 " R    Reset viewing parameters.",
-" T    Toggle additional viewing transforms.",
 " e    Toggle facet edge drawing.",
+#if 0
+" E    Toggle SOME TRIPLET.", /* might be specified */
+#endif
 " B    Toggle display of boundary facets.",
 " v    Toggle ridge and valley coloring.",
 " w    Toggle facets with three vertices on constraints.",
+" T    Toggle additional viewing transforms.",
 " b    Draw bounding box.",
-" +,- Increment, decrement fill color.",
+" +,-  Increment, decrement fill color.",
+#if 0
 " H    Toggle hidden surface removal.",
+#endif
 " t    Set clipping mode for torus.",
-" ?,h help (this display)",
-" x,q Exit to main menu."
+" ?,h  Display this help.",
+" x,q  Exit to main menu."
 };
                           
 void graph_help()
